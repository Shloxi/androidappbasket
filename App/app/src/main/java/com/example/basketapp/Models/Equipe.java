package com.example.basketapp.Models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Equipe")
public class Equipe {
    @PrimaryKey()
    @NonNull
    @ColumnInfo(name="idEquipe")
    public int idEquipe;

    @NonNull
    @ColumnInfo(name = "conf")
    public String conf;

    @NonNull
    @ColumnInfo(name = "nom")
    public String nom;

    @NonNull
    @ColumnInfo(name = "ville")
    public String ville;

    @NonNull
    @ColumnInfo(name = "nbVictoire")
    public int nbVictoire;

    @NonNull
    @ColumnInfo(name = "nbDefaite")
    public int nbDefaite;

    @NonNull
    @ColumnInfo(name = "nbTrophee")
    public int nbTrophee;

    @ColumnInfo(name = "idCoach")
    public int idCoach;

    public Equipe(@NonNull int idEquipe, @NonNull String conf, @NonNull String nom, @NonNull String ville, @NonNull int nbVictoire, @NonNull int nbDefaite, @NonNull int nbTrophee, @NonNull int idCoach) {
        this.idEquipe = idEquipe;
        this.conf = conf;
        this.nom = nom;
        this.ville = ville;
        this.nbVictoire = nbVictoire;
        this.nbDefaite = nbDefaite;
        this.nbTrophee = nbTrophee;
        this.idCoach = idCoach;
    }

    public int getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(int idEquipe) {
        this.idEquipe = idEquipe;
    }

    @NonNull
    public String getConf() {
        return conf;
    }

    public void setConf(@NonNull String conf) {
        this.conf = conf;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    public void setNom(@NonNull String nom) {
        this.nom = nom;
    }

    @NonNull
    public String getVille() {
        return ville;
    }

    public void setVille(@NonNull String ville) {
        this.ville = ville;
    }

    public int getNbVictoire() {
        return nbVictoire;
    }

    public void setNbVictoire(int nbVictoire) {
        this.nbVictoire = nbVictoire;
    }

    public int getNbDefaite() {
        return nbDefaite;
    }

    public void setNbDefaite(int nbDefaite) {
        this.nbDefaite = nbDefaite;
    }

    public int getNbTrophee() {
        return nbTrophee;
    }

    public void setNbTrophee(int nbTrophee) {
        this.nbTrophee = nbTrophee;
    }

    public int getIdCoach() {
        return idCoach;
    }

    public void setIdCoach(int idCoach) {
        this.idCoach = idCoach;
    }
}
