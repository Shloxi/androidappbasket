package com.example.basketapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.basketapp.BD.CoachViewModel;
import com.example.basketapp.BD.EquipeViewModel;
import com.example.basketapp.BD.JoueurViewModel;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.rechercheJoueur) {
            EditText editJoueur = findViewById(R.id.editJoueur);
            Intent i = new Intent(this, ActivityRecherche.class);
            i.putExtra("nomJoueur", editJoueur.getText().toString());
            startActivity(i);
        }
        else {
            Intent i = new Intent(this, ActivityConf.class);
            if (view.getId()==R.id.confEst) {
                i.putExtra("Conf","Est");
            }
            else if (view.getId()==R.id.confOuest) {
                i.putExtra("Conf", "Ouest");
            }
            startActivity(i);
        }
    }
}
