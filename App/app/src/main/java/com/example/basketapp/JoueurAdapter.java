package com.example.basketapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basketapp.Models.Joueur;

import java.util.List;

public class JoueurAdapter extends RecyclerView.Adapter<JoueurAdapter.MyViewHolder2> {

    private final List<Joueur> joueurs;
    private AppCompatActivity activity;

    JoueurAdapter(List<Joueur> joueurs, AppCompatActivity activity) {
        this.joueurs = joueurs;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.rv_joueurs, parent, false);
        return new MyViewHolder2(view,activity);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder2 holder, int position) {
        Joueur joueur = this.joueurs.get(position);
        holder.display(joueur);
    }

    @Override
    public int getItemCount() {
        return this.joueurs.size();
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView prenom;
        private final AppCompatActivity activity;

        private Joueur currentJoueur;

        public MyViewHolder2(final View itemView, AppCompatActivity activity) {
            super(itemView);
            this.activity = activity;
            this.name = ((TextView) itemView.findViewById(R.id.name));
            this.prenom = ((TextView) itemView.findViewById(R.id.prenom));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity.getBaseContext(), ActivityJoueur.class);
                    i.putExtra("idJoueur", currentJoueur.getIdJoueur());
                    activity.startActivity(i);
                }
            });
        }

        public void display(Joueur joueur) {
            this.currentJoueur = joueur;
            this.name.setText(joueur.getPrenom() + " " + joueur.getNom());
            this.prenom.setText("Numéro : " + Integer.toString(joueur.getNum()));
        }
    }
}
