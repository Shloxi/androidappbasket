package com.example.basketapp.BD;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.basketapp.Models.Coach;

import java.util.List;

public class CoachViewModel extends AndroidViewModel {
    private CoachRepository coachRepository;

    public CoachViewModel(@NonNull Application application) {
        super(application);
        this.coachRepository = new CoachRepository(application);
    }

    public void insert(Coach coach){
        this.coachRepository.insertCoach(coach);
    }

    public void deleteById(int id){ this.coachRepository.deleteById(id); }

    public void deleteAll(){
        this.coachRepository.deleteAll();
    }

    public Coach getCoach(Integer idCoach) { return this.coachRepository.getCoach(idCoach); }
}
