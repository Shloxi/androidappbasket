package com.example.basketapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basketapp.BD.EquipeDAO;
import com.example.basketapp.BD.EquipeViewModel;
import com.example.basketapp.Models.Equipe;

import java.util.List;

public class ActivityConf extends AppCompatActivity {

    private EquipeViewModel equipeVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conf);
        this.equipeVM = new EquipeViewModel(this.getApplication());

        String conf = getIntent().getExtras().getString("Conf");
        List<Equipe> equipes = this.equipeVM.afficheByConf(conf);

        RecyclerView viewConf = (RecyclerView)  findViewById(R.id.teamView);
        viewConf.setLayoutManager(new LinearLayoutManager(this));
        viewConf.setAdapter(new EquipeAdapter(equipes, this));
    }
}
