package com.example.basketapp.BD;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.basketapp.Models.Joueur;
import com.example.basketapp.Models.Coach;
import com.example.basketapp.Models.Equipe;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Joueur.class, Equipe.class, Coach.class}, version = 2)
public abstract class BasketRoomDatabase extends RoomDatabase {
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public abstract JoueurDAO joueurDAO();
    public abstract CoachDAO coachDAO();
    public abstract EquipeDAO equipeDAO();

    private static BasketRoomDatabase INSTANCE;

    static BasketRoomDatabase getDatabase(final Context context) {
        if(INSTANCE == null) {
            synchronized (BasketRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            BasketRoomDatabase.class, "database")
                            .fallbackToDestructiveMigration()
                            .addCallback(callback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback callback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            new AddTasks(INSTANCE).execute();
        }
    };

    private static class AddTasks extends AsyncTask<Void,Void,Void> {
        private  JoueurDAO joueurDAO;
        private  EquipeDAO equipeDAO;
        private  CoachDAO coachDAO;

        public AddTasks(BasketRoomDatabase db){
            joueurDAO = db.joueurDAO();
            coachDAO = db.coachDAO();
            equipeDAO = db.equipeDAO();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //Equipe ouest
            this.equipeDAO.insert(new Equipe(1,"Ouest", "Lakers", "Los angeles", 54, 28, 17, 1));
            this.equipeDAO.insert(new Equipe(2,"Ouest", "Rockets", "Houston", 47, 35, 2, 2));
            this.equipeDAO.insert(new Equipe(3,"Ouest", "Golden state Warriors", "San Francisco", 52, 30, 6, 3));
            this.equipeDAO.insert(new Equipe(7,"Ouest", "Thunder", "Oklahoma city", 48, 34, 1, 7));
            this.equipeDAO.insert(new Equipe(8,"Ouest", "Clippers", "Los angeles", 67, 15, 17, 8));
            this.equipeDAO.insert(new Equipe(11,"Ouest", "Spurs", "San Antonio", 35, 47, 6, 11));
            this.equipeDAO.insert(new Equipe(12,"Ouest", "Mavericks", "Dallas", 61, 21, 1, 12));
            this.equipeDAO.insert(new Equipe(13,"Ouest", "Trail Blazers", "Portland", 24, 58, 0, 13));
            this.equipeDAO.insert(new Equipe(14,"Ouest", "Nuggets", "Denver", 59, 23, 1, 14));
            this.equipeDAO.insert(new Equipe(15,"Ouest", "Timberwolves", "Minnesota", 17, 45, 0, 15));

            //Equipe est
            this.equipeDAO.insert(new Equipe(4,"Est", "Bucks", "Milwaukee", 53, 29, 8, 4));
            this.equipeDAO.insert(new Equipe(5,"Est", "Nets", "Brooklyn", 40, 42, 1, 5));
            this.equipeDAO.insert(new Equipe(6,"Est", "Sixiers", "Philadelphie", 45, 37, 2, 6));
            this.equipeDAO.insert(new Equipe(9,"Est", "Bulls", "Chicago", 38, 44, 6, 9));
            this.equipeDAO.insert(new Equipe(10,"Est", "Celtics", "Boston", 59, 23, 18, 10));
            this.equipeDAO.insert(new Equipe(16,"Est", "Raptors", "Toronto", 59, 23, 1, 16));
            this.equipeDAO.insert(new Equipe(17,"Est", "Heat", "Miami", 45, 37, 3, 17));
            this.equipeDAO.insert(new Equipe(18,"Est", "Hawks", "Atlanta", 30, 52, 1, 18));
            this.equipeDAO.insert(new Equipe(19,"Est", "Magic", "Orlando", 42, 40, 0, 19));
            this.equipeDAO.insert(new Equipe(20,"Est", "Pacers", "Indiana", 38, 44, 0, 20));

            //--------Joueur ouest-------

            //Lakers
            this.joueurDAO.insert(new Joueur("James", "LeBron", 23, 34, 4, 3,203,115, 25.7,7.9,10.6, 1));
            this.joueurDAO.insert(new Joueur("Antetokounmpo", "Kostas", 37, 21, 0, 0,205,104,0,0.3,0.3, 1));
            this.joueurDAO.insert(new Joueur("Bradley", "Avery", 11, 31, 0, 1, 196,98,8.6,2.3,1.3,1));
            this.joueurDAO.insert(new Joueur("Cacok", "Devontae", 12, 28, 0, 0, 204,100,0,0,0,1));
            this.joueurDAO.insert(new Joueur("Caldwell-pope", "Kentavious", 1, 36, 0, 0,196,95,9.5,2.1,1.7, 1));
            this.joueurDAO.insert(new Joueur("Caruso", "Alex", 4, 27, 0, 0, 190,87,5.4,1.9,1.8,1));
            this.joueurDAO.insert(new Joueur("Cook", "Quinn", 28, 25, 0, 1, 192,89,4.8,1.1,1.1,1));
            this.joueurDAO.insert(new Joueur("Davis", "Anthony", 3, 32, 0, 0, 215,120,26.7,9.4,3.1,1));
            this.joueurDAO.insert(new Joueur("Dudley", "Jared", 10, 34, 0, 0, 200,99,1.5,1.1,0.5,1));
            this.joueurDAO.insert(new Joueur("Green", "Dany", 14, 31, 0, 2, 198,94,8.2,3.4,1.4,1));
            this.joueurDAO.insert(new Joueur("Horton-Tucker", "Talen", 5, 36, 0, 0, 185,82,0,0,1.0,1));
            this.joueurDAO.insert(new Joueur("Howard", "Dwight", 39, 37, 0, 0, 213,120,7.5,7.4,0.7,1));
            this.joueurDAO.insert(new Joueur("Kuzma", "Kyle", 0, 23, 0, 0, 201,96,12.5,4.5,1.3,1));
            this.joueurDAO.insert(new Joueur("McGee", "JaVale", 7, 35, 0, 1, 211,108,6.8,5.8,0.6,1));
            this.joueurDAO.insert(new Joueur("Morris", "Markieff", 88, 33, 0, 0,199,99,10.1,3.8,1.4, 1));
            this.joueurDAO.insert(new Joueur("Rondo", "Rajon", 9, 36, 0, 1,188,88,7.1,3.0,5.0, 1));
            this.joueurDAO.insert(new Joueur("Waiters", "Dion", 18, 26, 0, 0, 193,92,9.3,3.7,1.0,1));


            //Houston
            this.joueurDAO.insert(new Joueur("Harden", "James", 13, 29, 1, 0, 197,98,34.4,6.4,7.4,2));
            this.joueurDAO.insert(new Joueur("Westbrook", "Russell", 0, 31, 1, 0, 191,95,27.5,8.0,7.0,2));
            this.joueurDAO.insert(new Joueur("Caboclo", "Bruno", 5, 23, 0, 0, 203,100,2.7,2.0,0.4,2));
            this.joueurDAO.insert(new Joueur("Carroll", "DeMarre", 9, 29, 0, 0, 205,97,3.5,2.2,0.9,2));
            this.joueurDAO.insert(new Joueur("Chandler", "Tyson", 19, 36, 0, 0, 209,108,1.3,2.5,0.2,2));
            this.joueurDAO.insert(new Joueur("Clemon", "Chris", 3, 27, 0, 0, 178,82,4.9,0.8,0.8,2));
            this.joueurDAO.insert(new Joueur("Covington", "Robert", 3, 28, 0, 0, 198,96,12.8,6.4,1.2,2));
            this.joueurDAO.insert(new Joueur("Frazier", "Michael", 21, 32, 0, 0, 190,90,1.8,0.4,0.1,2));
            this.joueurDAO.insert(new Joueur("Gordon", "Eric", 10, 27, 0, 0, 198,95,14.5,1.9,1.5,2));
            this.joueurDAO.insert(new Joueur("Green", "Jeff", 32, 30, 0, 0, 203,102,8.4,2.8,0.8,2));
            this.joueurDAO.insert(new Joueur("Hartenstein", "Isaiah", 55, 26, 0, 0,211,104,4.7,3.9,0.8, 2));
            this.joueurDAO.insert(new Joueur("House Jr", "Danuel", 4, 25, 0, 0, 198,93,10.2,4.2,1.3,2));
            this.joueurDAO.insert(new Joueur("Howard", "William", 52, 32, 0, 0, 192,87,0,1.0,0.5,2));
            this.joueurDAO.insert(new Joueur("McLemore", "Ben", 16, 28, 0, 0, 193,84,9.8,2.2,0.9,2));
            this.joueurDAO.insert(new Joueur("Rivers", "Austin", 25, 31, 0, 0,189,86,8.5,2.4,1.6, 2));
            this.joueurDAO.insert(new Joueur("Sefolosha", "Thabo", 18, 33, 0, 0,210,101,2.2,2.3,0.6, 2));
            this.joueurDAO.insert(new Joueur("Tucker", "PJ", 17, 29, 0, 0, 201,97,7.1,6.9,1.6,2));


            //Warriors
            this.joueurDAO.insert(new Joueur("Curry", "Stephen", 30, 31, 2, 3, 191,89,20.8,5.2,6.6,3));
            this.joueurDAO.insert(new Joueur("Bender", "Dragan", 12, 24, 0, 0, 206,102,6.7,4.6,1.8,3));
            this.joueurDAO.insert(new Joueur("Chriss", "Marquese", 32, 33, 0, 0,191,88,7.4,2.7,2.9, 3));
            this.joueurDAO.insert(new Joueur("Green", "Draymond", 23, 32, 0, 3, 211,105,9.3,6.2,1.9,3));
            this.joueurDAO.insert(new Joueur("Lee", "Damion", 1, 29, 0, 0, 206,97,12.7,4.9,2.7,3));
            this.joueurDAO.insert(new Joueur("Looney", "Kevon", 5, 34, 0, 2, 208,87,5.1,4.2,1.8,3));
            this.joueurDAO.insert(new Joueur("Mulder", "Mychal", 12, 27, 0, 0, 189,82,1.3,2.0,0.8,3));
            this.joueurDAO.insert(new Joueur("Paschall", "Eric", 7, 28, 0, 0, 200,91,2.5,6.4,3.4,3));
            this.joueurDAO.insert(new Joueur("Poole", "Jordan", 3, 24, 0, 0, 193,88,5.9,5.2,3.4,3));
            this.joueurDAO.insert(new Joueur("Randle", "Chasson", 9, 30, 0, 0, 208,98,2.4,5.8,3.4,3));
            this.joueurDAO.insert(new Joueur("Smailagic", "Alen", 6, 29, 0, 0, 203,94,2.5,3.7,4.9,3));
            this.joueurDAO.insert(new Joueur("Thompson", "Klay", 11, 32, 0, 3, 198,88,17.6,5.7,6.8,3));
            this.joueurDAO.insert(new Joueur("Toscano-Anderson", "Juan", 95, 36, 0, 0,210,104,2.3,6.1,1.0, 3));
            this.joueurDAO.insert(new Joueur("Wiggins", "Andrew", 22, 23, 0, 0, 198,90,2.6,4.1,1.8,3));


            //Thunder
            this.joueurDAO.insert(new Joueur("Adams", "Steven", 12, 31, 0, 0, 213,110,14.5,9.5,5.3,7));
            this.joueurDAO.insert(new Joueur("Bazley", "Darius", 7, 33, 0, 0, 195,85,3.6,4.3,2.8,7));
            this.joueurDAO.insert(new Joueur("Burton", "Deonte", 30, 28, 0, 0, 188,81,5.2,2.9,4.4,7));
            this.joueurDAO.insert(new Joueur("Diallo", "Hamidou", 6, 23, 0, 0, 194,86,3.4,6.2,1.9,7));
            this.joueurDAO.insert(new Joueur("Dort", "Lugentz", 5, 27, 0, 0,191,90,9.0,5.0,4.2, 7));
            this.joueurDAO.insert(new Joueur("Ferguson", "Terrance", 23, 22, 0, 0, 198,97,7.8,5.1,9.1,7));
            this.joueurDAO.insert(new Joueur("Gallinari", "Danilo", 8, 30, 0, 0, 207,94,20.4,3.6,7.2,7));
            this.joueurDAO.insert(new Joueur("Gilgeous-Alexander", "Shai", 2, 23, 0, 0, 198,85,23.1,5.7,4,7));
            this.joueurDAO.insert(new Joueur("Hervey", "Kevin", 15, 33, 0, 0, 188,80,2.1,3.4,7.2,7));
            this.joueurDAO.insert(new Joueur("Muscala", "Mike", 33, 32, 0, 0, 194,92,5.0,4.5,0.9,7));
            this.joueurDAO.insert(new Joueur("Nader", "Abdel", 11, 31, 0, 0, 185,85,6.4,7.0,8.2,7));
            this.joueurDAO.insert(new Joueur("Noel", "Nerlens", 9, 32, 0, 0,213,102,5.3,4.1,8.6, 7));
            this.joueurDAO.insert(new Joueur("Paul", "Chris", 3, 35, 0, 0, 183,83,25.2,4.3,10.5,7));
            this.joueurDAO.insert(new Joueur("Roberson", "Andre", 21, 30, 0, 0, 193,87,0,0,0,7));
            this.joueurDAO.insert(new Joueur("Roby", "Isaiah", 22, 34, 0, 0, 196,87,4.5,3.6,7.8,7));
            this.joueurDAO.insert(new Joueur("Schroder", "Dennis", 17, 28, 0, 0, 193,82,15.3,3.1,6.7,7));


            //Clippers
            this.joueurDAO.insert(new Joueur("Beverley", "Patrick", 21, 32, 0, 0, 193,80,10.6,5.6,7.4,8));
            this.joueurDAO.insert(new Joueur("Coffey", "Amir", 7, 26, 0, 0, 193,85,2.3,1.0,3.4,8));
            this.joueurDAO.insert(new Joueur("George", "Paul", 13, 32, 0, 0,206,97,20.5,8.4,6.1, 8));
            this.joueurDAO.insert(new Joueur("Green", "JaMychal", 4, 33, 0, 0, 210,105,2.3,8.7,6.3,8));
            this.joueurDAO.insert(new Joueur("Harrell", "Montrezl", 5, 34, 0, 0, 198,85,6.7,4.8,1.9,8));
            this.joueurDAO.insert(new Joueur("Jackson", "Reggie", 1, 32, 0, 0, 193,91,3.6,5.8,4.7,8));
            this.joueurDAO.insert(new Joueur("Kabengele", "Mfiondu", 25, 27, 0, 0, 213,105,4.6,8.7,6.4,8));
            this.joueurDAO.insert(new Joueur("Leonard", "Kawhi", 2, 30, 0, 2, 203,96,27.9,8.9,7.5,8));
            this.joueurDAO.insert(new Joueur("Mann", "Terance", 14, 31, 0, 0, 198,89,2.1,0.5,4.3,8));
            this.joueurDAO.insert(new Joueur("McGruder", "Rodney", 19, 29, 0, 0, 197,86,5.6,3.4,2.9,8));
            this.joueurDAO.insert(new Joueur("Curry", "Stephen", 30, 31, 0, 0, 196,87,5.6,3.2,0.7,8));
            this.joueurDAO.insert(new Joueur("Morris", "Marcus", 31, 33, 0, 0, 215,108,2.3,8.7,4.6,8));
            this.joueurDAO.insert(new Joueur("Motley", "Johnathan", 15, 30, 0, 0, 204,89,2.3,1.8,4.4,8));
            this.joueurDAO.insert(new Joueur("Noah", "Joakim", 55, 34, 0, 0, 215,100,2.3,6.3,3.7,8));
            this.joueurDAO.insert(new Joueur("Patterson", "Patrick", 54, 32, 0, 0,218,109,2.3,5.6,8.9, 8));
            this.joueurDAO.insert(new Joueur("Shamet", "Landry", 20, 24, 0, 0, 196,85,8.1,1.8,4.4,8));
            this.joueurDAO.insert(new Joueur("Williams", "Lou", 23, 35, 0, 0, 192,81,9.5,4.6,7.0,8));
            this.joueurDAO.insert(new Joueur("Zubac", "Ivica", 40, 28, 0, 0, 207,99,6.1,2.1,7.0,8));


            //Spurs
            this.joueurDAO.insert(new Joueur("Aldridge", "LaMarcus", 12, 33, 0, 0, 211,104,20.4,8.9,4.3,11));
            this.joueurDAO.insert(new Joueur("Belinelli", "Marco", 18, 30, 0, 0, 193,85,12.3,3.4,4.0,11));
            this.joueurDAO.insert(new Joueur("DeRozan", "DeMar", 10, 29, 0, 0,201,98,17.3,5.6,7.3, 11));
            this.joueurDAO.insert(new Joueur("Eubanks", "Drew", 14, 28, 0, 0, 203,94,2.1,2.3,0.4,11));
            this.joueurDAO.insert(new Joueur("Forbes", "Bryn", 11, 24, 0, 0, 210,102,3.4,2.1,0,11));
            this.joueurDAO.insert(new Joueur("Gay", "Rudy", 22, 27, 0, 0, 198,96,10.6,5.3,4.1,11));
            this.joueurDAO.insert(new Joueur("Johnson", "Keldon", 3, 30, 0, 0,191,89,2.1,0,4.3, 11));
            this.joueurDAO.insert(new Joueur("Lyles", "Trey", 41, 32, 0, 0, 185,83,0.6,3.2,4.6,11));
            this.joueurDAO.insert(new Joueur("Metu", "Chemezie", 7, 34, 0, 0, 198,90,2.3,6.3,3.1,11));
            this.joueurDAO.insert(new Joueur("Mills", "Patty", 8, 35, 0, 2, 188,85,7.9,2.3,8.7,11));
            this.joueurDAO.insert(new Joueur("Murray", "Dejunte", 5, 25, 0, 0, 193,83,10.6,5.6,7.3,11));
            this.joueurDAO.insert(new Joueur("Poeltl", "Jakob", 25, 27, 0, 0, 214,103,2.3,7.6,0.7,11));
            this.joueurDAO.insert(new Joueur("Samanic", "Luka", 19, 31, 0, 0, 196,87,2.4,6.4,9.4,11));
            this.joueurDAO.insert(new Joueur("Walker", "Lonnie", 1, 22, 0, 0, 194,85,4.6,3.8,5.1,11));
            this.joueurDAO.insert(new Joueur("Weatherspoon", "Quinndary", 15, 24, 0, 0, 206,99,3.2,0.4,0.7,11));
            this.joueurDAO.insert(new Joueur("White", "Derrick", 4, 28, 0, 0, 199,87,2.0,3.1,4.8,11));


            //Mavericks
            this.joueurDAO.insert(new Joueur("Barea", "JJ", 5, 28, 0, 0, 193,88,5.6,4.3,8.2,12));
            this.joueurDAO.insert(new Joueur("Brunson", "Jalen", 13, 30, 0, 0, 192,87,2.3,1.5,6.3,12));
            this.joueurDAO.insert(new Joueur("Cauley_Stein", "Willie", 33, 32, 0, 0, 205,99,3.6,4.9,5.2,12));
            this.joueurDAO.insert(new Joueur("Cleveland", "Antonius", 3, 33, 0, 0,215,102,2.6,6.3,4.1, 12));
            this.joueurDAO.insert(new Joueur("Curry", "Seth", 30, 30, 0, 0, 191,88,9.3,5.3,4.1,12));
            this.joueurDAO.insert(new Joueur("Doncic", "Luka", 77, 20, 0, 0, 193,85,25.3,7.2,8.2,12));
            this.joueurDAO.insert(new Joueur("Finney-Smith", "Dorian", 10, 25, 0, 0, 213,103,2.3,1.0,1.8,12));
            this.joueurDAO.insert(new Joueur("Hardaway Jr", "Tim", 11, 24, 0, 0,204,100,2.3,5.4,9.4, 12));
            this.joueurDAO.insert(new Joueur("Jackson", "Justin", 44, 27, 0, 0, 195,86,3.2,2.8,4.7,12));
            this.joueurDAO.insert(new Joueur("Kidd-Gilchrist", "Michael", 9, 33, 0, 0, 198,97,3.4,2.5,8.1,12));
            this.joueurDAO.insert(new Joueur("Kleber", "Maxi", 42, 26, 0, 0,208,200,0.5,0.4,2.1, 12));
            this.joueurDAO.insert(new Joueur("Lee", "Courtney", 1, 28, 0, 0,192,87,2.4,6.7,5.8, 12));
            this.joueurDAO.insert(new Joueur("Marjanovic", "Boban", 51, 34, 0, 0, 221,107,3.1,5.7,4.1,12));
            this.joueurDAO.insert(new Joueur("Powell", "Dwight", 7, 29, 0, 0, 191,87,2.3,4.8,7.1,12));
            this.joueurDAO.insert(new Joueur("Reaves", "Josh", 23, 24, 0, 0, 189,87,3.6,2.1,5.4,12));
            this.joueurDAO.insert(new Joueur("Wright", "Delon", 55, 35, 0, 0, 188,82,2.1,1.5,1.0,12));
            this.joueurDAO.insert(new Joueur("Porzingis", "Kristaps", 6, 23, 0, 0, 221,105,22.6,9.7,4.3,12));


            //Trail Blazers
            this.joueurDAO.insert(new Joueur("Anthony", "Carmelo", 00, 35, 0, 0, 203,98,10.2,5.3,6.4,13));
            this.joueurDAO.insert(new Joueur("Ariza", "Trevor", 8, 35, 0, 0, 203,95,6.4,4.6,7.3,13));
            this.joueurDAO.insert(new Joueur("Brown", "Moses", 4, 26, 0, 0, 211,104,3.0,2.4,1.9,13));
            this.joueurDAO.insert(new Joueur("Gabriel", "Wenyen", 35, 30, 0, 0, 213,99,2.1,4.9,8.2,13));
            this.joueurDAO.insert(new Joueur("Hezonja", "Mario", 44, 27, 0, 0,198,92,2.8,4.6,3.0, 13));
            this.joueurDAO.insert(new Joueur("Hoard", "Jaylen", 6, 31, 0, 0, 197,96,4.7,7.0,0.8,13));
            this.joueurDAO.insert(new Joueur("Hood", "Rodney", 5, 30, 0, 0, 198,88,2.1,0.5,4.6,13));
            this.joueurDAO.insert(new Joueur("Lillard", "Damian", 0, 29, 0, 0,191,90,28.7,5.5,8.9, 13));
            this.joueurDAO.insert(new Joueur("Little", "Nassir", 9, 33, 0, 0, 201,103,6.4,8.3,3.4,13));
            this.joueurDAO.insert(new Joueur("McCollum", "CJ", 3, 30, 0, 0, 198,98,20.3,4.6,7.5,13));
            this.joueurDAO.insert(new Joueur("Nurkic", "Jusuf", 27, 28, 0, 0, 212,104,15.6,9.7,3.4,13));
            this.joueurDAO.insert(new Joueur("Simons", "Anfernee", 1, 24, 0, 0, 198,87,3.2,1.6,5.4,13));
            this.joueurDAO.insert(new Joueur("Swanigan", "Caleb", 50, 35, 0, 0, 203,98,2.1,1.0,0.9,13));
            this.joueurDAO.insert(new Joueur("Trent Jr", "Gary", 2, 27, 0, 0, 189,85,3.6,8.5,2.4,13));
            this.joueurDAO.insert(new Joueur("Whiteside", "Hassan", 21, 28, 0, 0, 211,102,10.3,9.4,3.4,13));
            this.joueurDAO.insert(new Joueur("Collins", "Zach", 33, 30, 0, 0, 205,97,2.3,4.5,2.2,13));


            //Nuggets
            this.joueurDAO.insert(new Joueur("Barton", "Will", 5, 29, 0, 0, 201,98,8.9,6.3,4.1,14));
            this.joueurDAO.insert(new Joueur("Bates-Diop", "Keita", 31, 30, 0, 0, 198,86,2.3,0.8,1.4,14));
            this.joueurDAO.insert(new Joueur("Bol", "Bol", 10, 22, 0, 0, 231,112,2.2,8.3,2.3,14));
            this.joueurDAO.insert(new Joueur("Cancar", "Vlatko", 31, 26, 0, 0, 198,89,3.4,0.8,0.7,14));
            this.joueurDAO.insert(new Joueur("Craig", "Torey", 3, 28, 0, 0, 189,85,0.9,5.5,2.8,14));
            this.joueurDAO.insert(new Joueur("Daniel", "Troy", 30, 24, 0, 0, 198,87,8.7,6.3,2.1,14));
            this.joueurDAO.insert(new Joueur("Dozier", "PJ", 35, 26, 0, 0, 197,92,2.2,0.9,0.4,14));
            this.joueurDAO.insert(new Joueur("Grant", "Jerami", 9, 28, 0, 0, 208,99,7.5,6.7,9.1,14));
            this.joueurDAO.insert(new Joueur("Harris", "Gary", 14, 25, 0, 0, 198,90,2.2,1.3,3.4,14));
            this.joueurDAO.insert(new Joueur("Jokic", "Nikola", 15, 26, 0, 0, 213,105,20.7,8.7,9.7,14));
            this.joueurDAO.insert(new Joueur("Millsap", "Paul", 4, 32, 0, 0, 211,103,15.3,6.4,4.8,14));
            this.joueurDAO.insert(new Joueur("Morris", "Monte", 11, 28, 0, 0, 189,84,3.3,2.8,1.7,14));
            this.joueurDAO.insert(new Joueur("Murray", "Jamal", 27, 26, 0, 0, 198,95,14.2,0.8,6.4,14));
            this.joueurDAO.insert(new Joueur("Plumlee", "Mason", 7, 30, 0, 0, 213,104,5.6,7.8,2.4,14));
            this.joueurDAO.insert(new Joueur("Porter Jr", "Michael", 1, 22, 0, 0, 192,87,3.4,0.8,2.6,14));
            this.joueurDAO.insert(new Joueur("Vonleh", "Noah", 32, 27, 0, 0, 210,107,3.2,2.8,4.0,14));


            //Timberwolves
            this.joueurDAO.insert(new Joueur("Beasley", "Malik", 5, 29, 0, 0, 214,108,3.2,2.1,2.4,15));
            this.joueurDAO.insert(new Joueur("Culver", "Jarrett", 23, 24, 0, 0, 201,98,2.5,5.3,4.9,15));
            this.joueurDAO.insert(new Joueur("Evans", "Jacob", 12, 28, 0, 0, 197,94,2.4,6.4,5.1,15));
            this.joueurDAO.insert(new Joueur("Hernangomez", "Juhancho", 41, 23, 0, 0, 208,99,3.1,1.8,5.4,15));
            this.joueurDAO.insert(new Joueur("Johnson", "James", 16, 32, 0, 0, 201,95,4.6,6.2,8.3,15));
            this.joueurDAO.insert(new Joueur("Layman", "Jake", 10, 27, 0, 0, 193,86,0.4,2.1,1.0,15));
            this.joueurDAO.insert(new Joueur("Martin", "Kelan", 30, 26, 0, 0, 203,96,2.3,2.0,5.4,15));
            this.joueurDAO.insert(new Joueur("Mclaughlin", "Jordan", 6, 31, 0, 0, 199,91,6.5,2.0,1.4,15));
            this.joueurDAO.insert(new Joueur("Nowell", "Jaylen", 4, 29, 0, 0, 201,85,3.1,3.3,5.4,15));
            this.joueurDAO.insert(new Joueur("Okogie", "Josh", 20, 23, 0, 0, 193,87,3.3,4.2,0.7,15));
            this.joueurDAO.insert(new Joueur("Reid", "Naz", 11, 34, 0, 0, 207,99,2.4,5.6,6.0,15));
            this.joueurDAO.insert(new Joueur("Russell", "Dangelo", 0, 24, 0, 0, 191,88,21.3,5.3,7.9,15));
            this.joueurDAO.insert(new Joueur("Spellman", "Omari", 14, 36, 0, 0, 197,94,4.0,2.1,2.3,15));
            this.joueurDAO.insert(new Joueur("Towns", "Karl-Anthony", 32, 24, 0, 0, 213,108,20.6,10.8,4.2,15));
            this.joueurDAO.insert(new Joueur("Turner", "Evan", 1, 27, 0, 0, 198,98,2.1,5.6,6.1,15));
            this.joueurDAO.insert(new Joueur("Vanderbilt", "Jarred", 3, 31, 0, 0, 194,89,2.4,5.3,6.1,15));



            //--------Joueur est-------

            //Bucks
            this.joueurDAO.insert(new Joueur("Antetokounmpo", "Giannis", 34, 26, 1, 0, 211,110,31.2,10.8,7.6,4));
            this.joueurDAO.insert(new Joueur("Antetokounmpo", "Thanasis", 43, 22, 0, 0,204,98,4.3,3.6,2.4, 4));
            this.joueurDAO.insert(new Joueur("Bledsoe", "Eric", 6, 25, 0, 0, 191,85,10.3,4.6,7.9,4));
            this.joueurDAO.insert(new Joueur("Brown", "Sterling", 23, 27, 0, 0, 197,91,2.1,2.3,4.1,4));
            this.joueurDAO.insert(new Joueur("Connaughton", "Pat", 24, 28, 0, 0, 194,89,5.3,2.4,3.0,4));
            this.joueurDAO.insert(new Joueur("DiVincenzo", "Donte", 0, 30, 0, 0, 189,82,3.1,5.0,1.7,4));
            this.joueurDAO.insert(new Joueur("Hill", "George", 3, 32, 0, 0, 196,85,2.4,2.0,1.2,4));
            this.joueurDAO.insert(new Joueur("Ilyasova", "Ersan", 7, 29, 0, 0, 211,92,3.2,2.6,2.9,4));
            this.joueurDAO.insert(new Joueur("Korver", "Kyle", 26, 36, 0, 0, 191,81,7.9,2.8,6.1,4));
            this.joueurDAO.insert(new Joueur("Lopez", "Brook", 11, 27, 0, 0, 211,104,12.7,9.4,3.5,4));
            this.joueurDAO.insert(new Joueur("Lopez", "Robin", 42, 27, 0, 0, 212,106,5.6,7.3,4.2,4));
            this.joueurDAO.insert(new Joueur("Mason", "Frank", 15, 27, 0, 0, 193,89,6.4,2.6,4.8,4));
            this.joueurDAO.insert(new Joueur("Matthews", "Wesley", 9, 30, 0, 0, 191,93,8.9,6.2,4.8,4));
            this.joueurDAO.insert(new Joueur("Middleton", "khris", 22, 28, 0, 0, 194,90,15.6,4.8,6.7,4));
            this.joueurDAO.insert(new Joueur("Reynolds", "Cameron", 13, 33, 0, 0, 198,86,3.1,2.0,1.5,4));
            this.joueurDAO.insert(new Joueur("Williams", "Marvin", 20, 35, 0, 0, 204,94,3.1,5.0,4.6,4));
            this.joueurDAO.insert(new Joueur("Wilson", "DJ", 5, 31, 0, 0, 200,100,5.1,0.4,2.4,4));


            //Nets
            this.joueurDAO.insert(new Joueur("Durant", "Kevin", 7, 32, 1, 2, 210,95,29.5,7.6,9.4,5));
            this.joueurDAO.insert(new Joueur("Allen", "Jarett", 31, 28, 0, 0, 211,103,12.4,9.4,4.3,5));
            this.joueurDAO.insert(new Joueur("Chandler", "Wilson", 21, 33, 0, 0, 198,91,2.6,4.8,2.1,5));
            this.joueurDAO.insert(new Joueur("Chiozza", "Chris", 9, 30, 0, 0, 203,95,4.8,4.3,0.9,5));
            this.joueurDAO.insert(new Joueur("Claxton", "Nicolas", 33, 35, 0, 0, 214,106,1.7,5.3,3.7,5));
            this.joueurDAO.insert(new Joueur("Dinwiddie", "Spencer", 26, 29, 0, 0, 191,94,9.8,3.4,7.2,5));
            this.joueurDAO.insert(new Joueur("Harris", "Joe", 12, 28, 0, 0,193,83,2.6,4.8,7.0, 5));
            this.joueurDAO.insert(new Joueur("Irving", "Kyrie", 11, 29, 0, 1, 191,82,24.9,3.8,8.7,5));
            this.joueurDAO.insert(new Joueur("Jordan", "Deandre", 6, 30, 0, 0, 213,112,10.9,12.5,3.8,5));
            this.joueurDAO.insert(new Joueur("Kurucs", "Rodions", 00, 31, 0, 0, 203,95,2.4,2.8,4.8,5));
            this.joueurDAO.insert(new Joueur("LeVert", "Caris", 22, 26, 0, 0,188,80,13.4,4.6,6.9, 5));
            this.joueurDAO.insert(new Joueur("Luwawu-Cabarrot", "Timothe", 9, 35, 0, 0, 189,85,2.3,2.0,6.1,5));
            this.joueurDAO.insert(new Joueur("Martin", "Jeremiah", 0, 33, 0, 0, 195,90,6.4,5.0,0.6,5));
            this.joueurDAO.insert(new Joueur("Musa", "Dzanan", 13, 26, 0, 0, 207,99,2.4,4.6,3.7,5));
            this.joueurDAO.insert(new Joueur("Pinson", "Theo", 1, 28, 0, 0, 208,100,2.1,6.4,3.0,5));
            this.joueurDAO.insert(new Joueur("Prince", "Taurean", 2, 34, 0, 0, 195,90,6.1,1.5,1.7,5));
            this.joueurDAO.insert(new Joueur("Temple", "Garrett", 17, 35, 0, 0, 190,84,6.2,3.4,7.0,5));


            //Sixiers
            this.joueurDAO.insert(new Joueur("Embiid", "Joel", 21, 26, 0, 0, 215,110,24.8,14.3,6.2,6));
            this.joueurDAO.insert(new Joueur("Burck", "Alec", 20, 28, 0, 0, 190,85,6.4,4.3,5.7,6));
            this.joueurDAO.insert(new Joueur("Harris", "Tobias", 12, 27, 0, 0, 203,103,18.3,3.4,5.9,6));
            this.joueurDAO.insert(new Joueur("Horford", "Al", 42, 30, 0, 0, 194,86,2.3,6.1,1.4,6));
            this.joueurDAO.insert(new Joueur("Korkmaz", "Furkan", 30, 29, 0, 0,216,108,3.4,2.9,1.5, 6));
            this.joueurDAO.insert(new Joueur("Milton", "Shake", 18, 23, 0, 0, 207,98,1.5,0.6,0.8,6));
            this.joueurDAO.insert(new Joueur("Neto", "Raul", 19, 22, 0, 0, 187,80,2.3,5.0,4.1,6));
            this.joueurDAO.insert(new Joueur("O'Quinn", "Kyle", 9, 32, 0, 0, 210,105,6.3,4.9,2.8,6));
            this.joueurDAO.insert(new Joueur("Pelle", "Norvel", 14, 31, 0, 0, 200,100,6.1,1.5,0.9,6));
            this.joueurDAO.insert(new Joueur("Richardson", "Josh", 0, 29, 0, 0, 198,95,4.1,1.6,3.5,6));
            this.joueurDAO.insert(new Joueur("Robinson", "Glenn", 22, 24, 0, 0, 205,103,2.6,6.1,3.7,6));
            this.joueurDAO.insert(new Joueur("Scott", "Mike", 1, 28, 0, 0, 208,99,2.1,0.3,0.5,6));
            this.joueurDAO.insert(new Joueur("Shayok", "Marial", 35, 33, 0, 0, 190,84,2.1,1.2,3.4,6));
            this.joueurDAO.insert(new Joueur("Simmons", "Ben", 25, 23, 0, 0, 201,98,17.9,8.4,11.6,6));
            this.joueurDAO.insert(new Joueur("Smith", "Zhaire", 5, 23, 0, 0, 188,88,9.7,6.7,4.9,6));
            this.joueurDAO.insert(new Joueur("Thybulle", "Matisse", 22, 25, 0, 0,199,92,3.1,1.8,4.0, 6));


            //Bulls
            this.joueurDAO.insert(new Joueur("Arcidiacono", "Ryan", 51, 22, 0, 0, 209,99,6.4,7.5,3.1,9));
            this.joueurDAO.insert(new Joueur("Carter", "Wendell", 34, 24, 0, 0, 211,104,12.7,10.9,6.1,9));
            this.joueurDAO.insert(new Joueur("Dunn", "Kris", 32, 24, 0, 0,191,91,10.6,4.6,8.3, 9));
            this.joueurDAO.insert(new Joueur("Felicio", "Cristiano", 6, 25, 0, 0, 195,95,3.6,4.8,5.1,9));
            this.joueurDAO.insert(new Joueur("Gafford", "Daniel", 12, 26, 0, 0, 187,85,2.1,0.6,3.4,9));
            this.joueurDAO.insert(new Joueur("Harrison", "Shaquille", 3, 25, 0, 0, 200,100,2.0,1.6,0.7,9));
            this.joueurDAO.insert(new Joueur("Hutchison", "Chandler", 15, 24, 0, 0, 211,106,3.2,5.1,4.2,9));
            this.joueurDAO.insert(new Joueur("Kornet", "Luke", 2, 27, 0, 0, 198,99,2.0,1.7,0.9,9));
            this.joueurDAO.insert(new Joueur("Lavine", "Zach", 8, 26, 0, 0, 198,87,18.4,6.4,7.4,9));
            this.joueurDAO.insert(new Joueur("Markkanen", "Lauri", 24, 24, 0, 0, 208,95,15.6,6.4,1.2,9));
            this.joueurDAO.insert(new Joueur("Mokoka", "Adam", 20, 30, 0, 0, 192,86,3.6,2.7,1.8,9));
            this.joueurDAO.insert(new Joueur("Porter", "Otto", 22, 29, 0, 0, 203,102,2.3,1.9,4.2,9));
            this.joueurDAO.insert(new Joueur("Satoransky", "Tomas", 31, 30, 0, 0, 191,85,2.7,1.9,3.8,9));
            this.joueurDAO.insert(new Joueur("Strus", "Max", 28, 26, 0, 0, 184,90,0.8,1.2,0.7,9));
            this.joueurDAO.insert(new Joueur("Valentine", "Denzel", 45, 26, 0, 0,195,90,12.4,6.3,3.7, 9));
            this.joueurDAO.insert(new Joueur("White", "Coby", 0, 21, 0, 0, 191,87,10.6,3.7,4.3,9));
            this.joueurDAO.insert(new Joueur("Young", "Thaddeus", 21, 30, 0, 0, 211,84,3.2,1.5,2.8,9));




            //Celtics
            this.joueurDAO.insert(new Joueur("Brown", "Jaylen", 7, 23, 0, 0, 198,95,18.2,6.4,5.3,10));
            this.joueurDAO.insert(new Joueur("Edwards", "Carsen", 4, 28, 0, 0, 203,98,3.1,4.5,2.0,10));
            this.joueurDAO.insert(new Joueur("Fall", "Tacko", 99, 21, 0, 0, 231,110,5.6,7.8,4.2,10));
            this.joueurDAO.insert(new Joueur("Green", "Javonte", 43, 32, 0, 0, 198,90,0.3,2.4,3.1,10));
            this.joueurDAO.insert(new Joueur("Hayward", "Gordon", 20, 28, 0, 0, 203,87,17.3,6.4,7.2,10));
            this.joueurDAO.insert(new Joueur("Kanter", "Enes", 11, 33, 0, 0, 213,105,4.6,3.4,4.2,10));
            this.joueurDAO.insert(new Joueur("Langford", "Romeo", 45, 30, 0, 0, 192,89,4.3,0.5,0.9,10));
            this.joueurDAO.insert(new Joueur("Ojeleye", "Semi", 37, 25, 0, 0, 201,84,3.2,1.5,2.7,10));
            this.joueurDAO.insert(new Joueur("Poirier", "Vincent", 77, 31, 0, 0, 215,108,2.4,6.4,3.1,10));
            this.joueurDAO.insert(new Joueur("Smart", "Marcus", 36, 32, 0, 0, 188,84,7.9,4.5,9.3,10));
            this.joueurDAO.insert(new Joueur("Tatum", "Jayson", 0, 24, 0, 0, 198,95,18.4,6.1,7.4,10));
            this.joueurDAO.insert(new Joueur("Theis", "Daniel", 27, 33, 0, 0, 195,95,3.1,2.4,1.0,10));
            this.joueurDAO.insert(new Joueur("Walker", "Kemba", 8, 29, 0, 0, 188,82,20.1,6.1,7.2,10));
            this.joueurDAO.insert(new Joueur("Wanamaker", "Brad", 9, 31, 0, 0, 205,96,4.6,3.1,1.7,10));
            this.joueurDAO.insert(new Joueur("Waters", "Tremont", 51, 33, 0, 0, 210,103,1.6,3.4,2.5,10));
            this.joueurDAO.insert(new Joueur("Williams", "Grant", 12, 35, 0, 0, 208,102,6.4,2.3,1.8,10));
            this.joueurDAO.insert(new Joueur("Williams", "Robert", 44, 32, 0, 0, 198,97,5.6,4.8,2.9,10));


            //Raptors
            this.joueurDAO.insert(new Joueur("Anunoby", "OG", 3, 30, 0, 0, 190,90,1.3,2.5,4.6,16));
            this.joueurDAO.insert(new Joueur("Boucher", "Chris", 25, 32, 0, 0, 195,96,2.3,3.4,5.2,16));
            this.joueurDAO.insert(new Joueur("Brissett", "Oshae", 12, 29, 0, 0, 203,98,6.4,2.3,4.1,16));
            this.joueurDAO.insert(new Joueur("Davis", "Terence", 0, 33, 0, 0, 198,98,5.6,1.3,4.3,16));
            this.joueurDAO.insert(new Joueur("Gasol", "Marc", 33, 33, 0, 1, 197,96,4.3,2.5,3.1,16));
            this.joueurDAO.insert(new Joueur("Hernandez", "Dewan", 20, 28, 0, 0, 193,90,3.2,5.1,1.3,16));
            this.joueurDAO.insert(new Joueur("Williams", "Robert", 44, 32, 0, 0, 200,97,3.2,5.6,4.8,16));
            this.joueurDAO.insert(new Joueur("Hollis-Jefferson", "Rondae", 4, 27, 0, 1, 205,98,8.4,7.5,6.1,16));
            this.joueurDAO.insert(new Joueur("Ibaka", "Serge", 9, 30, 0, 1, 208,102,8.4,12.4,6.3,16));
            this.joueurDAO.insert(new Joueur("Johnson", "Stanley", 5, 24, 0, 0, 203,97,2.6,1.8,3.7,16));
            this.joueurDAO.insert(new Joueur("Lowry", "Kyle", 7, 32, 0, 1, 193,85,17.3,3.4,10.8,16));
            this.joueurDAO.insert(new Joueur("McCaw", "Patrick", 22, 26, 0, 3, 206,85,3.4,2.8,7.4,16));
            this.joueurDAO.insert(new Joueur("Miller", "Malcom", 13, 25, 0, 0, 198,89,5.3,2.3,4.1,16));
            this.joueurDAO.insert(new Joueur("Powell", "Norman", 24, 27, 0, 1, 195,86,10.5,3.1,7.3,16));
            this.joueurDAO.insert(new Joueur("Siakam", "Pascal", 43, 29, 0, 1, 211,100,15.4,9.2,4.3,16));
            this.joueurDAO.insert(new Joueur("Thomas", "Matt", 21, 32, 0, 0, 188,85,2.3,0.5,0.7,16));
            this.joueurDAO.insert(new Joueur("VanVleet", "Fred", 23, 25, 0, 1, 188,87,10.6,3.1,8.1,16));
            this.joueurDAO.insert(new Joueur("Watson", "Paul", 1, 33, 0, 0, 199,92,5.3,4.2,1.9,16));




            //Heat
            this.joueurDAO.insert(new Joueur("Adebayo", "Bam", 13, 24, 0, 0, 208,96,14.6,5.3,6.7,17));
            this.joueurDAO.insert(new Joueur("Alexander", "Kyle", 17, 24, 0, 0,198,89,5.4,2.1,0.9, 17));
            this.joueurDAO.insert(new Joueur("Butler", "Jimmy", 22, 28, 0, 0, 201,92,20.5,6.4,7.2,17));
            this.joueurDAO.insert(new Joueur("Crowder", "Jae", 99, 35, 0, 0, 203,98,8.6,6.1,2.4,17));
            this.joueurDAO.insert(new Joueur("Dragic", "Goran", 7, 32, 0, 0, 188,80,6.7,5.3,9.1,17));
            this.joueurDAO.insert(new Joueur("Haslem", "Udonis", 40, 38, 0, 0, 215,100,2.3,3.1,0.5,17));
            this.joueurDAO.insert(new Joueur("Herro", "Tyler", 14, 22, 0, 0, 191,85,8.1,4.3,6.1,17));
            this.joueurDAO.insert(new Joueur("Hill", "Solomon", 44, 30, 0, 0, 210,103,3.4,2.0,0.78,17));
            this.joueurDAO.insert(new Joueur("Iguodala", "Andre", 28, 35, 0, 0, 206,100,6.1,3.1,8.0,17));
            this.joueurDAO.insert(new Joueur("Jones Jr", "Derrick", 5, 23, 0, 0, 213,98,6.4,7.3,5.2,17));
            this.joueurDAO.insert(new Joueur("Leonard", "Meyes", 0, 24, 0, 0, 215,110,2.1,1.6,5.2,17));
            this.joueurDAO.insert(new Joueur("Nunn", "Kendrick", 25, 31, 0, 0, 186,88,3.2,1.5,0.6,17));
            this.joueurDAO.insert(new Joueur("Okpala", "Kz", 4, 22, 0, 0, 187,80,1.6,2.4,3.8,17));
            this.joueurDAO.insert(new Joueur("Olynyk", "Kelly", 9, 30, 0, 0, 208,95,4.3,6.,2.1,17));
            this.joueurDAO.insert(new Joueur("Robinson", "Duncan", 55, 24, 0, 0, 191,84,4.6,3.5,7.2,17));
            this.joueurDAO.insert(new Joueur("Silva", "Chris", 30, 32, 0, 0, 198,91,1.8,6.9,4.3,17));
            this.joueurDAO.insert(new Joueur("Vincent", "Gabe", 2, 31, 0, 0, 193,93,2.6,1.4,0.3,17));



            //Hawks
            this.joueurDAO.insert(new Joueur("Bembry", "DeAndre", 95, 30, 0, 0, 198,95,2.3,4.3,6.1,18));
            this.joueurDAO.insert(new Joueur("Brown Jr", "Charles", 4, 27, 0, 0, 203,96,2.6,5.1,0.9,18));
            this.joueurDAO.insert(new Joueur("Capela", "Clint", 17, 28, 0, 0, 216,108,15.3,12.6,3.7,18));
            this.joueurDAO.insert(new Joueur("Carter", "Vince", 15, 41, 0, 0, 198,90,3.4,1.6,4.8,18));
            this.joueurDAO.insert(new Joueur("Collins", "John", 20, 24, 0, 0, 200,94,3.1,2.0,1.8,18));
            this.joueurDAO.insert(new Joueur("Dedmon", "Dewayne", 14, 29, 0, 0, 208,98,2.3,2.9,3.4,18));
            this.joueurDAO.insert(new Joueur("Fernando", "Bruno", 24, 28, 0, 0, 198,96,6.4,3.1,0.8,18));
            this.joueurDAO.insert(new Joueur("Goodwin", "Brandon", 0, 32, 0, 0, 191,92,4.6,3.5,2.0,18));
            this.joueurDAO.insert(new Joueur("Graham", "Treveon", 2, 33, 0, 0, 205,97,1.6,4.5,2.9,18));
            this.joueurDAO.insert(new Joueur("Huerter", "Kevin", 3, 23, 0, 0, 185,86,1.5,2.6,3.4,18));
            this.joueurDAO.insert(new Joueur("Hunter", "DeAndre", 12, 24, 0, 0, 206,102,9.1,4.6,2.8,18));
            this.joueurDAO.insert(new Joueur("Jones", "Damian", 30, 22, 0, 0, 198,89,4.3,1.9,5.7,18));
            this.joueurDAO.insert(new Joueur("Labissière", "Skal", 7, 26, 0, 0, 211,105,9.1,4.2,0.8,18));
            this.joueurDAO.insert(new Joueur("Reddish", "Cam", 22, 25, 0, 0,195,87,8.7,6.3,4.9, 18));
            this.joueurDAO.insert(new Joueur("Teague", "Jeff", 00, 30, 0, 0, 211,104,6.1,2.3,2.0,18));
            this.joueurDAO.insert(new Joueur("Young", "Trae", 11, 21, 0, 0,191,80,29.3,5.3,10.7, 18));


            //Magic
            this.joueurDAO.insert(new Joueur("Aminu", "Al-Farouq", 2, 32, 0, 0, 198,85,2.4,3.4,1.0,19));
            this.joueurDAO.insert(new Joueur("Augustin", "DJ", 14, 30, 0, 0, 211,109,7.6,8.1,4.9,19));
            this.joueurDAO.insert(new Joueur("Bamba", "Mo", 5, 21, 0, 0, 216,110,2.6,11.8,3.7,19));
            this.joueurDAO.insert(new Joueur("Birch", "Khem", 24, 29, 0, 0, 198,88,3.4,4.1,1.0,19));
            this.joueurDAO.insert(new Joueur("Carter-Williams", "Michael", 7, 27, 0, 0, 188,85,6.1,7.5,4.9,19));
            this.joueurDAO.insert(new Joueur("Clark", "Gary", 12, 31, 0, 0, 191,90,19.4,3.4,2.6,19));
            this.joueurDAO.insert(new Joueur("Ennis", "James", 11, 26, 0, 0, 198,96,6.1,7.3,8.4,19));
            this.joueurDAO.insert(new Joueur("Fournier", "Evan", 10, 33, 0, 0, 196,91,12.4,3.4,8.1,19));
            this.joueurDAO.insert(new Joueur("Frazier", "Melvin", 35, 30, 0, 0, 213,106,3.4,2.7,1.8,19));
            this.joueurDAO.insert(new Joueur("Fultz", "Markelle", 20, 22, 0, 0, 193,90,9.4,3.7,8.3,19));
            this.joueurDAO.insert(new Joueur("Gordon", "Aaron", 00, 28, 0, 0, 203,100,12.3,3.7,6.5,19));
            this.joueurDAO.insert(new Joueur("Isaac", "Jonathan", 1, 23, 0, 0, 215,123,2.9,6.4,4.8,19));
            this.joueurDAO.insert(new Joueur("Iwundu", "Wes", 25, 28, 0, 0, 198,98,4.9,8.5,3.7,19));
            this.joueurDAO.insert(new Joueur("Johnson", "BJ", 13, 27, 0, 0, 185,80,1.6,0.8,0.7,19));
            this.joueurDAO.insert(new Joueur("Law", "Vic", 23, 25, 0, 0, 182,80,4.6,3.1,0.8,19));
            this.joueurDAO.insert(new Joueur("Ross", "Terrence", 31, 36, 0, 0, 198,90,3.4,2.0,5.4,19));
            this.joueurDAO.insert(new Joueur("Vucevic", "Nikola", 9, 30, 0, 0, 210,100,14.6,8.9,6.1,19));


            //Pacers
            this.joueurDAO.insert(new Joueur("Bitadze", "Goga", 88, 30, 0, 0, 211,100,3.4,2.6,1.9,20));
            this.joueurDAO.insert(new Joueur("Bowen", "Brian", 10, 29, 0, 0, 198,90,2.5,4.6,9.1,20));
            this.joueurDAO.insert(new Joueur("Brogdon", "Malcom", 7, 24, 0, 0, 188,87,1.6,5.3,3.4,20));
            this.joueurDAO.insert(new Joueur("Holiday", "Aaron", 3, 31, 0, 0,215,108,6.1,8.6,2.7, 20));
            this.joueurDAO.insert(new Joueur("Holiday", "Justin", 8, 32, 0, 0, 200,95,3.4,4.1,4.0,20));
            this.joueurDAO.insert(new Joueur("Johnson", "Alize", 24, 28, 0, 0,197,86,2.6,5.8,4.9, 20));
            this.joueurDAO.insert(new Joueur("Lamb", "Jeremy", 26, 30, 0, 0, 208,100,3.4,7.2,0.9,20));
            this.joueurDAO.insert(new Joueur("Leaf", "TJ", 22, 24, 0, 0, 195,95,2.5,4.6,2.3,20));
            this.joueurDAO.insert(new Joueur("McConnell", "TJ", 9, 31, 0, 0, 201,90,3.6,5.2,8.0,20));
            this.joueurDAO.insert(new Joueur("McDermott", "Doug", 20, 34, 0, 0, 198,92,4.6,2.3,1.5,20));
            this.joueurDAO.insert(new Joueur("Mitrou-Long", "Naz", 15, 23, 0, 0, 196,84,3.4,5.8,2.1,20));
            this.joueurDAO.insert(new Joueur("Oladipo", "Victor", 4, 30, 0, 0, 198,89,21.6,4.6,8.3,20));
            this.joueurDAO.insert(new Joueur("Sabonis", "Domantas", 11, 24, 0, 0,208,94,12.3,7.3,2.9, 20));
            this.joueurDAO.insert(new Joueur("Sampson", "JaKarr", 14, 33, 0, 0, 188,88,2.6,8.1,7.3,20));
            this.joueurDAO.insert(new Joueur("Turner", "Myles", 33, 32, 0, 0, 216,110,3.7,8.1,2.0,20));
            this.joueurDAO.insert(new Joueur("Warren", "TJ", 1, 29, 0, 0, 192,93,3.4,7.2,0.8,20));



            //Coach ouest
            this.coachDAO.insert(new Coach("Vogel", "Frank", 46,304,291,0.511,"13 mai 2019" ,1));
            this.coachDAO.insert(new Coach("Dantoni", "Mike", 69, 628,499,0.557,"1 juin 2016",2));
            this.coachDAO.insert(new Coach("Kerr", "Steve", 71, 322,88,0.785,"19 mai 2014",3));
            this.coachDAO.insert(new Coach("Donovan", "Billy", 54, 199,129,0.607, "30 avril 2015",7));
            this.coachDAO.insert(new Coach("Rivers", "Doc", 58, 894,658,0.576,"25 juin 2013",8));
            this.coachDAO.insert(new Coach("Popovich", "Gregg", 63, 1245,575,0.684,"10 decembre 1996",11));
            this.coachDAO.insert(new Coach("Carlisle", "Rick", 52, 751,627,0.545,"9 mai 2008",12));
            this.coachDAO.insert(new Coach("Stotts", "Terry", 48, 440,417,0.513,"7 aout 2012",13));
            this.coachDAO.insert(new Coach("Malone", "Michael", 45, 212,222,0.488,"15 juin 2015",14));
            this.coachDAO.insert(new Coach("Sauders", "Ryan", 55, 17,25,0.405,"6 janvier 2019",15));

            //Coach est
            this.coachDAO.insert(new Coach("Budenholzer", "Mike", 51, 273,219,0.555,"18 mai 2018",4));
            this.coachDAO.insert(new Coach("Vaughn", "Jacque", 45, 58,158,0.269,"7 mars 2020",5));
            this.coachDAO.insert(new Coach("Brown", "Bret", 59, 178,314,0.362,"14 aout 2013",6));
            this.coachDAO.insert(new Coach("Boylen", "Jim", 55, 17,41,0.293,"3 decembre 2018",9));
            this.coachDAO.insert(new Coach("Stevens", "Brad", 43, 270,222,0.549,"3 juillet 2013",10));
            this.coachDAO.insert(new Coach("Nurse", "Nick", 42, 58,24,0.707,"14 juin 2018",16));
            this.coachDAO.insert(new Coach("Spoelstra", "Eric", 55, 523,363,0.590,"28 avril 2008",17));
            this.coachDAO.insert(new Coach("Pierce", "Lloyd", 49, 29,53,0.354,"11 mai 2018",18));
            this.coachDAO.insert(new Coach("Clifford", "Steve", 53, 238,254,0.484,"30 mai 2018",19));
            this.coachDAO.insert(new Coach("McMillan", "Nate", 57,616,560,0.524,"16 mai 2016", 20));

            return null;
        }
    }
}
