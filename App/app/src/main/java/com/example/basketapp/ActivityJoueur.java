package com.example.basketapp;

import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basketapp.BD.JoueurViewModel;
import com.example.basketapp.Models.Joueur;

import java.util.List;

public class ActivityJoueur extends AppCompatActivity {
    private JoueurViewModel joueurVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joueur);
        this.joueurVM = new JoueurViewModel(this.getApplication());

        Integer idJoueur = getIntent().getExtras().getInt("idJoueur");
        Joueur joueur = this.joueurVM.getJoueur(idJoueur);

        TextView name = (TextView) findViewById(R.id.name);
        name.setText(joueur.getPrenom() + " " + joueur.getNom());
        TextView num = (TextView) findViewById(R.id.numJoueur);
        num.setText("Numéro : " + Integer.toString(joueur.getNum()));
        TextView age = (TextView) findViewById(R.id.ageJoueur);
        age.setText("Age : " + Integer.toString(joueur.getAge()));
        TextView mvp = (TextView) findViewById(R.id.mvpJoueur);
        mvp.setText("Mvp : " + Integer.toString(joueur.getNbMVP()));
        TextView titre = (TextView) findViewById(R.id.titreJoueur);
        titre.setText("Titre : " + Integer.toString(joueur.getNbTitre()));
        TextView taille = (TextView) findViewById(R.id.tailleJoueur);
        taille.setText("Taille : " + joueur.getTaille());
        TextView poids = (TextView) findViewById(R.id.poidsJoueur);
        poids.setText("Poids : " + Integer.toString(joueur.getPoids()));
        TextView points = (TextView) findViewById(R.id.pointsMatchJoueur);
        points.setText("Points par match : " + Double.toString(joueur.getPointParMatch()));
        TextView rebond = (TextView) findViewById(R.id.rebondMatchJoueur);
        rebond.setText("Rebond par match : " + Double.toString(joueur.getRebondParMatch()));
        TextView assist = (TextView) findViewById(R.id.assistMatchJoueur);
        assist.setText("Assist par match : " + Double.toString(joueur.getAssistParMatch()));
    }
}
