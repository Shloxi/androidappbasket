package com.example.basketapp.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.basketapp.Models.Equipe;

import java.util.List;

@Dao
public interface EquipeDAO {

    @Insert
    void insert(Equipe equipe);

    @Query("DELETE FROM Equipe")
    void deleteAll();

    @Query("DELETE FROM EQUIPE WHERE idEquipe=:id")
    void deleteId(int id);

    @Query("SELECT * from Equipe where conf=:conf")
    List<Equipe> selectByConf(String conf);

    @Query("SELECT * FROM Equipe where idEquipe=:idEquipe")
    Equipe getEquipe(int idEquipe);

    @Query("SELECT * FROM Equipe")
    LiveData<List<Equipe>> selectAll();
}
