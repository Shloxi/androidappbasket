package com.example.basketapp.Models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.Entity;

@Entity(tableName = "Joueur")
public class Joueur {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="idJoueur")
    public int idJoueur;

    @NonNull
    @ColumnInfo(name = "nom")
    public String nom;

    @NonNull
    @ColumnInfo(name = "prenom")
    public String prenom;

    @NonNull
    @ColumnInfo(name = "num")
    public int num;

    @NonNull
    @ColumnInfo(name = "age")
    public int age;

    @NonNull
    @ColumnInfo(name = "nbMVP")
    public int nbMVP;

    @NonNull
    @ColumnInfo(name = "nbTitre")
    public int nbTitre;

    @NonNull
    @ColumnInfo(name = "taille")
    public int taille;

    @NonNull
    @ColumnInfo(name = "poids")
    public int poids;

    @NonNull
    @ColumnInfo(name = "pointParMatch")
    public double pointParMatch;

    @NonNull
    @ColumnInfo(name = "rebondParMatch")
    public double rebondParMatch;

    @NonNull
    @ColumnInfo(name = "assistParMatch")
    public double assistParMatch;

    @ColumnInfo(name= "idEquipe")
    public int idEquipe;

    public Joueur(@NonNull String nom, @NonNull String prenom, @NonNull int num, @NonNull int age, @NonNull int nbMVP, @NonNull int nbTitre, @NonNull int taille, @NonNull int poids, @NonNull double pointParMatch, @NonNull double rebondParMatch, @NonNull double assistParMatch, int idEquipe) {
        this.nom = nom;
        this.prenom = prenom;
        this.num = num;
        this.age = age;
        this.nbMVP = nbMVP;
        this.nbTitre = nbTitre;
        this.taille = taille;
        this.poids = poids;
        this.pointParMatch = pointParMatch;
        this.rebondParMatch = rebondParMatch;
        this.assistParMatch = assistParMatch;
        this.idEquipe = idEquipe;
    }

    public int getIdJoueur() {
        return idJoueur;
    }

    public void setIdJoueur(int idJoueur) {
        this.idJoueur = idJoueur;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    public void setNom(@NonNull String nom) {
        this.nom = nom;
    }

    @NonNull
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(@NonNull String prenom) {
        this.prenom = prenom;
    }

    public int getNum() { return num; }

    public void setNum(int num) { this.num = num; }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNbMVP() {
        return nbMVP;
    }

    public void setNbMVP(int nbMVP) {
        this.nbMVP = nbMVP;
    }

    public int getNbTitre() {
        return nbTitre;
    }

    public void setNbTitre(int nbTitre) {
        this.nbTitre = nbTitre;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public int getPoids() {
        return poids;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    public double getPointParMatch() {
        return pointParMatch;
    }

    public void setPointParMatch(int pointParMatch) {
        this.pointParMatch = pointParMatch;
    }

    public double getRebondParMatch() {
        return rebondParMatch;
    }

    public void setRebondParMatch(int rebondParMatch) {
        this.rebondParMatch = rebondParMatch;
    }

    public double getAssistParMatch() {
        return assistParMatch;
    }

    public void setAssistParMatch(int assistParMatch) {
        this.assistParMatch = assistParMatch;
    }

    public int getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(int idEquipe) {
        this.idEquipe = idEquipe;
    }
}
