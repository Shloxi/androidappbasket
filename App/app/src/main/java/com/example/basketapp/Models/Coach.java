package com.example.basketapp.Models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Coach")
public class Coach {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="idCoach")
    public int idCoach;

    @NonNull
    @ColumnInfo(name = "nom")
    public String nom;

    @NonNull
    @ColumnInfo(name = "prenom")
    public String prenom;

    @NonNull
    @ColumnInfo(name = "age")
    public int age;

    @NonNull
    @ColumnInfo(name = "nbMatchVictoire")
    public int nbMatchVictoire;

    @NonNull
    @ColumnInfo(name = "nbMatchDefaite")
    public int nbMatchDefaite;

    @NonNull
    @ColumnInfo(name = "victoirePourcent")
    public double victoirePourcent;

    @NonNull
    @ColumnInfo(name = "dateDebut")
    public String dateDebut;

    @ColumnInfo(name= "idEquipe")
    public int idEquipe;

    public Coach(@NonNull String nom, @NonNull String prenom, @NonNull int age, @NonNull int nbMatchVictoire, @NonNull int nbMatchDefaite, @NonNull double victoirePourcent, @NonNull String dateDebut, int idEquipe) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.nbMatchVictoire = nbMatchVictoire;
        this.nbMatchDefaite = nbMatchDefaite;
        this.victoirePourcent = victoirePourcent;
        this.dateDebut = dateDebut;
        this.idEquipe = idEquipe;
    }

    public int getIdCoach() {
        return idCoach;
    }

    public void setIdCoach(int idCoach) {
        this.idCoach = idCoach;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    public void setNom(@NonNull String nom) {
        this.nom = nom;
    }

    @NonNull
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(@NonNull String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNbMatchVictoire() {
        return nbMatchVictoire;
    }

    public void setNbMatchVictoire(int nbMatchVictoire) {
        this.nbMatchVictoire = nbMatchVictoire;
    }

    public int getNbMatchDefaite() {
        return nbMatchDefaite;
    }

    public void setNbMatchDefaite(int nbMatchDefaite) {
        this.nbMatchDefaite = nbMatchDefaite;
    }

    public double getVictoirePourcent() {
        return victoirePourcent;
    }

    public void setVictoirePourcent(int victoirePourcent) {
        this.victoirePourcent = victoirePourcent;
    }

    @NonNull
    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(@NonNull String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public int getIdEquipe() {
        return idEquipe;
    }

    public void setIdEquipe(int idEquipe) {
        this.idEquipe = idEquipe;
    }
}
