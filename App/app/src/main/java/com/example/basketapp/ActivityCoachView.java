package com.example.basketapp;

import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.basketapp.BD.CoachViewModel;
import com.example.basketapp.Models.Coach;

public class ActivityCoachView extends AppCompatActivity {
    private CoachViewModel coachVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coachview);
        this.coachVM = new CoachViewModel(this.getApplication());

        Integer idCoach = getIntent().getExtras().getInt("idCoach");
        Coach coach = this.coachVM.getCoach(idCoach);

        TextView nom = (TextView) findViewById(R.id.nomCoach);
        nom.setText(coach.getNom());
        TextView debut = (TextView) findViewById(R.id.debutCoach);
        debut.setText(coach.getDateDebut());
        TextView age = (TextView) findViewById(R.id.ageCoach);
        age.setText("Age : " + Integer.toString(coach.getAge()));
        TextView victoire = (TextView) findViewById(R.id.victoireCoach);
        victoire.setText("Nombre de Victoire : " + coach.getNbMatchVictoire());
        TextView defaite = (TextView) findViewById(R.id.defaiteCoach);
        defaite.setText("Nombre de Defaite : " + coach.getNbMatchDefaite());
        TextView pourcent = (TextView) findViewById(R.id.pourcentCoach);
        pourcent.setText("Pourcentage de victoire : " + Double.toString(coach.getVictoirePourcent()*100) + "%");
    }
}
