package com.example.basketapp.BD;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.basketapp.Models.Coach;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CoachRepository {
    private CoachDAO coachDAO;
    private LiveData<List<Coach>> allCoach;

    public CoachRepository(Application application) {
        BasketRoomDatabase db = BasketRoomDatabase.getDatabase(application);
        this.coachDAO = db.coachDAO();
        this.allCoach = coachDAO.selectAll();
    }

    public void insertCoach(Coach coach)  {
        new insertAsyncTask(coachDAO).execute(coach);
    }

    private class insertAsyncTask extends AsyncTask<Coach, Void, Void> {
        private CoachDAO coachDAO;
        insertAsyncTask(CoachDAO coachDAO) {this.coachDAO = coachDAO;}

        @Override
        protected Void doInBackground(final Coach... Coachs) {
            this.coachDAO.insert(Coachs[0]);
            return null;
        }
    }

    public void deleteAll() {new deleteAllAsyncTask(coachDAO).execute();}

    private class deleteAllAsyncTask extends AsyncTask<Coach, Void, Void> {
        private CoachDAO coachDAO;
        deleteAllAsyncTask(CoachDAO coachDAO) {this.coachDAO = coachDAO;}

        @Override
        protected Void doInBackground(final Coach... Coachs) {
            this.coachDAO.deleteAll();
            return null;
        }
    }

    public void deleteById(int id) {new deleteAsyncTask(coachDAO).execute(id);}

    private class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {
        private CoachDAO coachDAO;
        deleteAsyncTask(CoachDAO coachDAO) {this.coachDAO = coachDAO;}

        @Override
        protected Void doInBackground(final Integer... id) {
            this.coachDAO.deleteId(id[0]);
            return null;
        }
    }

    public Coach getCoach(Integer idCoach) {
        try {
            return new getCoachAsyncTask(coachDAO).execute(idCoach).get();
        } catch (ExecutionException | InterruptedException e) {
            Log.d("Mes logs", Arrays.toString(e.getStackTrace()));
        } return null; }

    private static class getCoachAsyncTask extends AsyncTask<Integer, Void, Coach> {
        private CoachDAO coachDAO;
        getCoachAsyncTask(CoachDAO coachDAO) {this.coachDAO = coachDAO;}

        @Override
        protected Coach doInBackground(Integer... integers) {
            Coach coach = this.coachDAO.getCoach(integers[0]);
            return coach;
        }
    }
}
