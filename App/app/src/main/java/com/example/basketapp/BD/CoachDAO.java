package com.example.basketapp.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.basketapp.Models.Coach;

import java.util.List;

@Dao
public interface CoachDAO {

    @Insert
    void insert(Coach coach);

    @Query("DELETE FROM Coach")
    void deleteAll();

    @Query("DELETE FROM COACH WHERE idCoach=:id")
    void deleteId(int id);

    @Query("SELECT * FROM Coach where idCoach=:idCoach")
    Coach getCoach(int idCoach);

    @Query("SELECT * FROM Coach")
    LiveData<List<Coach>> selectAll();
}
