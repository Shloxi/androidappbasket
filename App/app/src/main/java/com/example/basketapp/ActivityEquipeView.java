package com.example.basketapp;

import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.basketapp.BD.EquipeViewModel;
import com.example.basketapp.Models.Equipe;

public class ActivityEquipeView extends AppCompatActivity {
    private EquipeViewModel equipeVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipeview);
        this.equipeVM = new EquipeViewModel(this.getApplication());

        Integer idEquipe = getIntent().getExtras().getInt("idEquipe");
        Equipe equipe = this.equipeVM.getEquipe(idEquipe);

        TextView nom = (TextView) findViewById(R.id.nomEquipe);
        nom.setText(equipe.getNom());
        TextView ville = (TextView) findViewById(R.id.villeEquipe);
        ville.setText(equipe.getVille());
        TextView victoire = (TextView) findViewById(R.id.victoireEquipe);
        victoire.setText("Nombre de Victoire : " + equipe.getNbVictoire());
        TextView defaite = (TextView) findViewById(R.id.defaiteEquipe);
        defaite.setText("Nombre de Defaite : " + equipe.getNbDefaite());
        TextView trophee = (TextView) findViewById(R.id.tropheeEquipe);
        trophee.setText("Nombre de Trophée : " + equipe.getNbTrophee());
    }
}
