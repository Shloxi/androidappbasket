package com.example.basketapp;

import android.os.Bundle;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basketapp.BD.JoueurViewModel;
import com.example.basketapp.Models.Joueur;

import java.util.List;

public class ActivityRecherche extends AppCompatActivity {
    private JoueurViewModel joueurVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipe);
        this.joueurVM = new JoueurViewModel(this.getApplication());
        String nomJoueur = getIntent().getExtras().getString("nomJoueur");
        List<Joueur> joueurs = this.joueurVM.getJoueurByNom(nomJoueur);
        RecyclerView viewTeam = (RecyclerView)  findViewById(R.id.joueurView);
        viewTeam.setLayoutManager(new LinearLayoutManager(this));
        viewTeam.setAdapter(new JoueurAdapter(joueurs, this));
    }
}
