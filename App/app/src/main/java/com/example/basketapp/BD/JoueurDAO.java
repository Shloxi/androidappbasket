package com.example.basketapp.BD;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.SkipQueryVerification;

import com.example.basketapp.Models.Joueur;

import java.util.List;

@Dao
public interface JoueurDAO {

    @Insert
    void insert(Joueur joueur);

    @Query("DELETE FROM Joueur")
    void deleteAll();

    @Query("DELETE FROM JOUEUR WHERE idJoueur=:id")
    void deleteId(int id);

    @Query("SELECT * from Joueur where idEquipe=:id")
    List<Joueur> getJoueurByTeam(Integer id);

    @Query("SELECT * from Joueur where nom LIKE '%' || :nomJoueur || '%' or prenom LIKE '%' || :nomJoueur || '%'")
    List<Joueur> getJoueurByNom(String nomJoueur);

    @Query("SELECT * From Joueur where idJoueur=:id")
    Joueur getJoueur(Integer id);

    @Query("SELECT * FROM JOUEUR")
    LiveData<List<Joueur>> selectAll();
}
