package com.example.basketapp.BD;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.basketapp.Models.Joueur;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class JoueurRepository {
    private JoueurDAO joueurDAO;
    private LiveData<List<Joueur>> allJoueur;

    public JoueurRepository(Application application) {
        BasketRoomDatabase db = BasketRoomDatabase.getDatabase(application);
        this.joueurDAO = db.joueurDAO();
        this.allJoueur = joueurDAO.selectAll();
    }

    public void insertJoueur(Joueur joueur)  {
        new insertAsyncTask(joueurDAO).execute(joueur);
    }

    private class insertAsyncTask extends AsyncTask<Joueur, Void, Void> {
        private JoueurDAO joueurDAO;
        insertAsyncTask(JoueurDAO joueurDAO) {this.joueurDAO = joueurDAO;}

        @Override
        protected Void doInBackground(final Joueur... Joueurs) {
            this.joueurDAO.insert(Joueurs[0]);
            return null;
        }
    }

    public void deleteAll() {new deleteAllAsyncTask(joueurDAO).execute();}

    private class deleteAllAsyncTask extends AsyncTask<Joueur, Void, Void> {
        private JoueurDAO joueurDAO;
        deleteAllAsyncTask(JoueurDAO joueurDAO) {this.joueurDAO = joueurDAO;}

        @Override
        protected Void doInBackground(final Joueur... Joueurs) {
            this.joueurDAO.deleteAll();
            return null;
        }
    }

    public void deleteById(int id) {new deleteAsyncTask(joueurDAO).execute(id);}

    private class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {
        private JoueurDAO joueurDAO;
        deleteAsyncTask(JoueurDAO joueurDAO) {this.joueurDAO = joueurDAO;}

        @Override
        protected Void doInBackground(final Integer... id) {
            this.joueurDAO.deleteId(id[0]);
            return null;
        }
    }

    public List<Joueur> getJoueurByTeam(Integer idEquipe) {
        try {
            return new getJoueurByTeamAsyncTask(joueurDAO).execute(idEquipe).get();
        } catch (ExecutionException | InterruptedException e) {
            Log.d("Mes logs", Arrays.toString(e.getStackTrace()));
        } return null; }

    private static class getJoueurByTeamAsyncTask extends AsyncTask<Integer, Void, List<Joueur>> {
        private JoueurDAO joueurDAO;
        getJoueurByTeamAsyncTask(JoueurDAO joueurDAO) {this.joueurDAO = joueurDAO;}

        @Override
        protected List<Joueur> doInBackground(Integer... integers) {
            List<Joueur> joueurs = this.joueurDAO.getJoueurByTeam(integers[0]);
            return joueurs;
        }
    }

    public List<Joueur> getJoueurByNom(String nomJoueur) {
        try {
            return new getJoueurByNomAsyncTask(joueurDAO).execute(nomJoueur).get();
        } catch (ExecutionException | InterruptedException e) {
            Log.d("Mes logs", Arrays.toString(e.getStackTrace()));
        } return null; }

    private static class getJoueurByNomAsyncTask extends AsyncTask<String, Void, List<Joueur>> {
        private JoueurDAO joueurDAO;
        getJoueurByNomAsyncTask(JoueurDAO joueurDAO) {this.joueurDAO = joueurDAO;}

        @Override
        protected List<Joueur> doInBackground(String... strings) {
            List<Joueur> joueurs = this.joueurDAO.getJoueurByNom(strings[0]);
            return joueurs;
        }
    }

    public Joueur getJoueur(Integer idJoueur) {
        try {
            return new getJoueurAsyncTask(joueurDAO).execute(idJoueur).get();
        } catch (ExecutionException | InterruptedException e) {
            Log.d("Mes logs", Arrays.toString(e.getStackTrace()));
        } return null; }

    private static class getJoueurAsyncTask extends AsyncTask<Integer, Void, Joueur> {
        private JoueurDAO joueurDAO;
        getJoueurAsyncTask(JoueurDAO joueurDAO) {this.joueurDAO = joueurDAO;}

        @Override
        protected Joueur doInBackground(Integer... integers) {
            Joueur joueur = this.joueurDAO.getJoueur(integers[0]);
            return joueur;
        }
    }
}
