package com.example.basketapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basketapp.BD.JoueurViewModel;
import com.example.basketapp.Models.Joueur;

import java.util.List;

public class ActivityEquipe extends AppCompatActivity {

    private JoueurViewModel joueurVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipe);
        this.joueurVM = new JoueurViewModel(this.getApplication());
        Integer idEquipe = getIntent().getExtras().getInt("idEquipe");
        List<Joueur> joueurs = this.joueurVM.getJoueurByTeam(idEquipe);
        RecyclerView viewTeam = (RecyclerView)  findViewById(R.id.joueurView);
        viewTeam.setLayoutManager(new LinearLayoutManager(this));
        viewTeam.setAdapter(new JoueurAdapter(joueurs, this));
    }
}
