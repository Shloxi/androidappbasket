package com.example.basketapp.BD;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.basketapp.Models.Joueur;

import java.util.List;

public class JoueurViewModel extends AndroidViewModel {
    private JoueurRepository joueurRepository;

    public JoueurViewModel(@NonNull Application application) {
        super(application);
        this.joueurRepository = new JoueurRepository(application);
    }

    public void insert(Joueur joueur){
        this.joueurRepository.insertJoueur(joueur);
    }

    public void deleteById(int id){ this.joueurRepository.deleteById(id); }

    public void deleteAll(){
        this.joueurRepository.deleteAll();
    }

    public List<Joueur> getJoueurByTeam(Integer idEquipe) { return this.joueurRepository.getJoueurByTeam(idEquipe); }

    public List<Joueur> getJoueurByNom(String nomJoueur) { return this.joueurRepository.getJoueurByNom(nomJoueur); }

    public Joueur getJoueur(Integer idJoueur) { return this.joueurRepository.getJoueur(idJoueur); }
}
