package com.example.basketapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basketapp.Models.Equipe;

import java.util.List;

public class EquipeAdapter extends RecyclerView.Adapter<EquipeAdapter.MyViewHolder> {

    private final List<Equipe> equipes;
    private AppCompatActivity activity;

    EquipeAdapter(List<Equipe> equipes, AppCompatActivity activity) {
        this.equipes = equipes;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.rv_equipes, parent, false);
        return new MyViewHolder(view, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Equipe equipe = this.equipes.get(position);
        holder.display(equipe);
    }

    @Override
    public int getItemCount() {
        return this.equipes.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name;
        private final TextView trophee;
        private final Button equipe;
        private final Button coach;
        private final AppCompatActivity activity;

        private Equipe currentEquipe;

        public MyViewHolder(final View itemView, AppCompatActivity activity) {
            super(itemView);
            this.activity = activity;
            this.equipe = ((Button) itemView.findViewById(R.id.equipeButton));
            this.coach = ((Button) itemView.findViewById(R.id.coachButton));
            this.name = ((TextView) itemView.findViewById(R.id.name));
            this.trophee = ((TextView) itemView.findViewById(R.id.trophee));

            equipe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity.getBaseContext(), ActivityEquipeView.class);
                    i.putExtra("idEquipe", currentEquipe.getIdEquipe());
                    activity.startActivity(i);
                }
            });

            coach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity.getBaseContext(), ActivityCoachView.class);
                    i.putExtra("idCoach", currentEquipe.getIdCoach());
                    activity.startActivity(i);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(activity.getBaseContext(), ActivityEquipe.class);
                    i.putExtra("idEquipe", currentEquipe.getIdEquipe());
                    activity.startActivity(i);
                }
            });
        }

        public void display(Equipe equipe) {
            this.currentEquipe = equipe;
            this.name.setText(equipe.nom);
            this.trophee.setText("Nombre de trophée : " + Integer.toString(equipe.nbTrophee));
        }
    }
}
