package com.example.basketapp.BD;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.basketapp.Models.Equipe;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class EquipeRepository {
    private EquipeDAO equipeDAO;
    private LiveData<List<Equipe>> allEquipe;

    public EquipeRepository(Application application) {
        BasketRoomDatabase db = BasketRoomDatabase.getDatabase(application);
        this.equipeDAO = db.equipeDAO();
        this.allEquipe = equipeDAO.selectAll();
    }

    public void insertEquipe(Equipe equipe)  {
        new insertAsyncTask(equipeDAO).execute(equipe);
    }

    private static class insertAsyncTask extends AsyncTask<Equipe, Void, Void> {
        private EquipeDAO equipeDAO;
        insertAsyncTask(EquipeDAO equipeDAO) {this.equipeDAO = equipeDAO;}

        @Override
        protected Void doInBackground(final Equipe... equipes) {
            this.equipeDAO.insert(equipes[0]);
            return null;
        }
    }

    public void deleteAll() {new deleteAllAsyncTask(equipeDAO).execute();}

    private static class deleteAllAsyncTask extends AsyncTask<Equipe, Void, Void> {
        private EquipeDAO equipeDAO;
        deleteAllAsyncTask(EquipeDAO joueurDAO) {this.equipeDAO = equipeDAO;}

        @Override
        protected Void doInBackground(final Equipe... Equipes) {
            this.equipeDAO.deleteAll();
            return null;
        }
    }

    public void deleteById(int id) {new deleteAsyncTask(equipeDAO).execute(id);}

    private static class deleteAsyncTask extends AsyncTask<Integer, Void, Void> {
        private EquipeDAO equipeDAO;
        deleteAsyncTask(EquipeDAO equipeDAO) {this.equipeDAO = equipeDAO;}

        @Override
        protected Void doInBackground(final Integer... id) {
            this.equipeDAO.deleteId(id[0]);
            return null;
        }
    }

    public List<Equipe> afficheByConf(String conf) {
        try {
            return new afficheByConfAsyncTask(equipeDAO).execute(conf).get();
        } catch (ExecutionException | InterruptedException e) {
            Log.d("Mes logs", Arrays.toString(e.getStackTrace()));
        } return null; }

    private static class afficheByConfAsyncTask extends AsyncTask<String, Void, List<Equipe>> {
        private EquipeDAO equipeDAO;
        afficheByConfAsyncTask(EquipeDAO equipeDAO) {this.equipeDAO = equipeDAO;}

        @Override
        protected List<Equipe> doInBackground(String... strings) {
            List<Equipe> equipes = this.equipeDAO.selectByConf(strings[0]);
            return equipes;
        }
    }

    public Equipe getEquipe(Integer idEquipe) {
        try {
            return new getEquipeAsyncTask(equipeDAO).execute(idEquipe).get();
        } catch (ExecutionException | InterruptedException e) {
            Log.d("Mes logs", Arrays.toString(e.getStackTrace()));
        } return null; }

    private static class getEquipeAsyncTask extends AsyncTask<Integer, Void, Equipe> {
        private EquipeDAO equipeDAO;
        getEquipeAsyncTask(EquipeDAO equipeDAO) {this.equipeDAO = equipeDAO;}

        @Override
        protected Equipe doInBackground(Integer... integers) {
            Equipe equipe = this.equipeDAO.getEquipe(integers[0]);
            return equipe;
        }
    }
}
