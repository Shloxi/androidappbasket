package com.example.basketapp.BD;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.basketapp.Models.Equipe;

import java.util.List;

public class EquipeViewModel extends AndroidViewModel {
    private EquipeRepository equipeRepository;

    public EquipeViewModel(@NonNull Application application) {
        super(application);
        this.equipeRepository = new EquipeRepository(application);
    }

    public void insert(Equipe equipe){
        this.equipeRepository.insertEquipe(equipe);
    }

    public void deleteById(int id){ this.equipeRepository.deleteById(id); }

    public void deleteAll(){
        this.equipeRepository.deleteAll();
    }

    public List<Equipe> afficheByConf(String conf) { return this.equipeRepository.afficheByConf(conf); }

    public Equipe getEquipe(Integer idEquipe) { return this.equipeRepository.getEquipe(idEquipe); }
}
