CREATE TABLE JOUEUR (
    id Integer primary key,
    nom Text,
    prenom Text,
    age Integer,
    mvp Integer,
    nbTitre Integer,
    idEquipe Integer);

CREATE TABLE EQUIPE(
    conference Text,
    nom Text primary key,
    ville Text,
    nbVictoire Integer,
    nbDefaite Integer,
    nbTrophée Integer,
    idCoach Integer
    idEquipe Integer);

CREATE TABLE COACH(
    id Integer primary key,
    nom Text,
    prenom Text,
    age Integer,
    idEquipe Integer);


INSERT INTO COACH(id, nom, prenom, age, idEquipe) 
VALUES
    (1, "Kerr", "steve", 58, 1),
    (2, "Dany", "Ainge", 55, 2),
    (3, "D’Antoni", "Mike", 54, 3);

INSERT INTO EQUIPE(conference, nom, ville, nbVictoire, nbDefaite, nbTrophée, idCoach, idEquipe)
VALUES
    ("Ouest", "Golden state Warriors", "San Francisco", 20, 62, 5, 1, 1),
    ("Est",   "Celtics", "Boston", 51, 31, 18, 2, 2),
    ("Ouest", "Rockets", "Housto", 58, 24, 3, 3, 3);

INSERT INTO JOUEUR(id, nom, prenom, age, mvp, nbTitre, idEquipe)
VALUES
    (1, "Curry", "Stephen", 31, 2, 3, 1),
    (2, "Tatum", "Jayson", 22, 0, 0, 2),
    (3, "Harden", "James", 30, 1, 0, 3);
